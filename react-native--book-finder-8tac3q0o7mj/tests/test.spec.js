const books = [{
        "isbn": "9781593275846",
        "title": "Eloquent JavaScript, Second Edition",
    },
    {
        "isbn": "9781449331818",
        "title": "Learning JavaScript Design Patterns",
    },
    {
        "isbn": "9781449365035",
        "title": "Speaking JavaScript",
    },
    {
        "isbn": "9781491950296",
        "title": "Programming JavaScript Applications",
    },
    {
        "isbn": "9781593277574",
        "title": "Understanding ECMAScript 6",
    },
    {
        "isbn": "9781491904244",
        "title": "You Don't Know JS",
    },
    {
        "isbn": "9781449325862",
        "title": "Git Pocket Guide",
    },
    {
        "isbn": "9781449337711",
        "title": "Designing Evolvable Web APIs with ASP.NET",
    }
]
module.exports = {
    'App should list all books and isbn on app load in the same order': function (browser) {
        browser
            .url(browser.launch_url)
            .elements('css selector', '.cls-isbn', function (result) {
                this.verify.equal(result.value.length, books.length);
                for (let i = 0; i < books.length; i++) {
                    browser.elementIdText(result.value[i].ELEMENT, (ele) => {
                        if (ele.value.toLowerCase().includes(books[i].isbn)) {
                            browser.verify.equal(true, true)
                        } else {
                            browser.verify.equal(true, false)
                        }
                    })
                    if (i == books.length - 1) browser.end()
                }
            });
    },
    'App should list all books and title on app load in the same order': function (browser) {
        browser
            .url(browser.launch_url)
            .elements('css selector', '.cls-title', function (result) {
                this.verify.equal(result.value.length, books.length);
                for (let i = 0; i < books.length; i++) {
                    browser.elementIdText(result.value[i].ELEMENT, (ele) => {
                        if (ele.value.toLowerCase().includes(books[i].title.toLocaleLowerCase())) {
                            browser.verify.equal(true, true)
                        } else {
                            browser.verify.equal(true, false)
                        }
                    })
                    if (i == books.length - 1) browser.end()
                }
            });
    },
    'App should search by isbn': function (browser) {
        browser
            .url(browser.launch_url)
            .setValue('#txt-search', '978159')
            .elements('css selector', '.cls-isbn', function (result) {
                this.verify.equal(result.value.length, 2);
                browser.clearValue('#txt-search')
                    .setValue('#txt-search', '97814')
                    .elements('css selector', '.cls-isbn', function (res) {
                        this.verify.equal(res.value.length, 6);
                        this.end()
                    })
            });
    },
    'App should search by book title': function (browser) {
        browser
            .url(browser.launch_url)
            .setValue('#txt-search', 'java')
            .elements('css selector', '.cls-title', function (result) {
                this.verify.equal(result.value.length, 4);
                browser.clearValue('#txt-search')
                    .setValue('#txt-search', 'pattern')
                    .elements('css selector', '.cls-isbn', function (res) {
                        this.verify.equal(res.value.length, 1);
                        this.end()
                    })
            });
    },
};