import React, { Component } from 'react';
import {TextInput,View,Text,StyleSheet,FlatList} from 'react-native'
import books from './books.js';

//  add id='txt-search' to TextInput used take userInput
//  add className='cls-isbn' to Text component that displays isbn
//  add className='cls-title' to Text component that displays title
//  You can use FlatList for listing books

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      listData: books,
      searchedData: [],
    }
  }

  componentDidMount(){
    this.searchFilterFunction('');
  }

  searchFilterFunction = text => {    
    const newData = this.state.listData.filter(item => {      
      const itemData = `${item.isbn} ${item.title.toUpperCase()}`;
       const textData = text.toUpperCase();
        
       return itemData.indexOf(textData) > -1;    
    });    
    this.setState({ searchedData: newData });  
  };

  renderHeader = () => {
    return (      
      <TextInput  
        id='txt-search'      
        placeholder="Type Here..."             
        onChangeText={text => this.searchFilterFunction(text)}
        autoCorrect={false}
      />    
    );
  }
  
  render() {
    return (
      <View style={styles.wrapper}>
      <View style={styles.statusBar}><Text>Book Finder</Text></View>
      <View style={styles.container}>
        <FlatList
          data={this.state.searchedData}
          keyExtractor={(item, index) => index.toString()} 
          ListHeaderComponent={this.renderHeader}
          renderItem={({item}) => 
            <View>
              <Text className='cls-isbn'>ISBN: {item.isbn}</Text>
              <Text className='cls-title'>Title: {item.title}</Text>
            </View> 
          }
        />
      </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  wrapper:{
    width:'320px',
    height:'500px'
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  statusBar:{
    height:'40px',
    width:'100%',
    backgroundColor:'#99f',
    alignItems: 'center',
    justifyContent: 'center',
  }
})

export default App;