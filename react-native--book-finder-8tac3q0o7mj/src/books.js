export default
  [
      {
        "isbn": "9781593275846",
        "title": "Eloquent JavaScript, Second Edition",
      },
      {
        "isbn": "9781449331818",
        "title": "Learning JavaScript Design Patterns",
      },
      {
        "isbn": "9781449365035",
        "title": "Speaking JavaScript",
      },
      {
        "isbn": "9781491950296",
        "title": "Programming JavaScript Applications",
      },
      {
        "isbn": "9781593277574",
        "title": "Understanding ECMAScript 6",
      },
      {
        "isbn": "9781491904244",
        "title": "You Don't Know JS",
      },
      {
        "isbn": "9781449325862",
        "title": "Git Pocket Guide",
      },
      {
        "isbn": "9781449337711",
        "title": "Designing Evolvable Web APIs with ASP.NET",
      }
  ]