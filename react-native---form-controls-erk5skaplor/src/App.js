import React, { Component } from 'react';
import { Text, View, StyleSheet, TextInput, Picker} from 'react-native';

// add  id="txt-name" to TextInput
// add  className='picker-lang' to picker
// add className='txt-message' to Text that displays the message


export default class App extends Component {
  
  constructor(props){
    super(props);
    this.state = {
      name: "",
      language: "",
    }
  }
   
  render() {
    return (
      <View style={styles.container}>
          <View style={styles.statusBar}><Text>Form Controls</Text></View>
          <View>
            <TextInput 
            id="txt-name"
            placeholder="Enter your Name"
            value={this.state.name}
            onChangeText={(text)=> this.setState({name: text})}
            />
          </View>
          <View>
            <Picker
              className="picker-lang"
              selectedValue={this.state.language}
              style={{height: 50, width: 100}}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({language: itemValue})
              }>
              <Picker.Item label="Java" value="Java" />
              <Picker.Item label="JavaScript" value="JavaScript" />
              <Picker.Item label="React" value="React" />
              <Picker.Item label="React Native" value="React Native" />
            </Picker>
          </View>
          <View>
            {
              (this.state.name !== "" && this.state.language !== "") ? (
                <Text className="txt-message">{ this.state.name + ' likes '+ this.state.language}</Text>
              ) : (
                <View />
              )
            }
          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#eee',
    width:'300px',
    height:'500px'
  },
  statusBar:{
    height:'40px',
    width:'100%',
    backgroundColor:'#99f',
    alignItems: 'center',
    justifyContent: 'center',
  },
 
});
