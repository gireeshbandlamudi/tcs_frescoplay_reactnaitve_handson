module.exports = {

    'should display <name> likes <language> on text change and picker change': function (browser) {
        browser
            .url(browser.launch_url)
            .setValue('#txt-name', 'fresco')
            .click(`.picker-lang option[value=React]`)
            .verify.elementPresent('.txt-message')
            .verify.containsText('.txt-message', 'fresco')
            .verify.containsText('.txt-message', 'React')
            .end()
    },
    'should not display message if name is empty': function (browser) {
        browser
            .url(browser.launch_url)
            .clearValue('#txt-name')
            .click(`.picker-lang option[value=React]`)
            .verify.elementNotPresent('.txt-message')
            .end()
    },
    'should display <name> likes <language> on text change and multiple picker change': function (browser) {
        browser
            .url(browser.launch_url)
            .setValue('#txt-name', 'fresco')
            .click(`.picker-lang option[value=React]`)
            .verify.elementPresent('.txt-message')
            .verify.containsText('.txt-message', 'fresco')
            .verify.containsText('.txt-message', 'React')
            .click(`.picker-lang option[value=Java]`)
            .verify.containsText('.txt-message', 'fresco')
            .verify.containsText('.txt-message', 'Java')
            .end()
    },
};