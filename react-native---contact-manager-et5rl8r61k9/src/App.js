import React, { Component } from 'react';
import { Text, View, StyleSheet, TextInput, Button, FlatList } from 'react-native';

// add className='txt-name' to Text component that displays name (in list item)
// add className='txt-phone' to Text component that displays phone (in list item)
// add testID='btn-add' to Add Button
// add  id="txt-name" to TextInput that accepts user's name
// add  id="txt-phone" to TextInput that accepts user's phone number
// add  testID=btn-delete-0 for 1st contact-list delete Button and testID=btn-delete-1 to second contact-list delete Button and  so on


export default class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      name: "",
      contactNumber: "",
      refresh: false,
      contactsList: []
    }
  }

  onPressLearnMore = () => {
    const { name, contactNumber } = this.state;
    this.setState({
      name: "",
      contactNumber: "",
    })
    if(name === ""){
      return;
    }
    if(contactNumber === ""){
      return;
    } else{
      var temp = this.state.contactsList;
      temp.push({
        name: name,
        phone: contactNumber
      });
      this.setState({
        contactsList: temp,
        refresh: !this.state.refresh,
      })
    }
  }

  deleteContact = (name, contact) => {
    console.log(name, contact);
    var temp = this.state.contactsList;
    for(var i=0; i<temp.length; i++){
      if(temp[i].name === name && temp[i].phone === contact){
        temp.splice(i, 1);
        this.setState({
          contactsList: temp,
          refresh: !this.state.refresh,
        })
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.statusBar}><Text>Contacts</Text></View>
        <View style={styles.formView}>
            <View>
              <TextInput 
              id="txt-name"
              placeholder="Name"
              onChangeText={(text)=> this.setState({name: text})}
              value={this.state.name}
              />
            </View>
            <View>
              <TextInput 
              id="txt-phone"
              placeholder="Contact Number"
              onChangeText={(text)=> this.setState({contactNumber: text})}
              value={this.state.contactNumber}
              keyboardType="numeric"
              maxLength={10}
              />
            </View>
            <View>
            <Button
                testID='btn-add'
                onPress={()=> this.onPressLearnMore()}
                title="ADD"
                color="#841584"
                accessibilityLabel="ADD"
                disabled={(this.state.name.length < 3 || this.state.contactNumber.length !== 10)}
              />
            </View>
        </View>
        <View>
          <FlatList
            extraData={this.state.refresh}
            data={this.state.contactsList}
            keyExtractor={(item, index) => index.toString()} 
            renderItem={({item, index}) => 
              <View style={{flexDirection: 'row'}}>
                <View>
                  <Text className='txt-name'>Name: {item.name}</Text>
                  <Text className='txt-phone'>Phone: {item.phone}</Text>
                </View>
                <View>
                  <Button
                  testID={"btn-delete-"+index}
                  onPress={()=> this.deleteContact(item.name, item.phone)}
                  title="DELETE"
                  color="#841584"
                  accessibilityLabel="ADD"
                  />
                </View>
              </View> 
            }
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#eee',
    width:'300px',
    height:'500px'
  },
  statusBar:{
    height:'40px',
    width:'100%',
    backgroundColor:'#99f',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formView: {
    borderBottomWidth: 1,
    borderColor: '#ccc',
    paddingBottom: 8,
  },
});
