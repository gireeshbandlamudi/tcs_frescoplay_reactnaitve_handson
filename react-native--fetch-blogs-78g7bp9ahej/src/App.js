import React,{Component} from 'react';
import { Text, View,StyleSheet, FlatList} from 'react-native';
const API = `https://my-json-server.typicode.com/frescoplaylab/vue-e2-db/db`


// add className="txt-item" to list item

export default class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      blogData: [],
    }
  }

  async componentDidMount(){
    await fetch(API)
    .then((response) => response.json())
    .then((result) => {
      console.log(result);
      this.setState({
        blogData: result.blogs,
      })
    })
  }

  render(){
    return(
      <View style={styles.container}>
        <View style={styles.statusBar} >
          <Text>Blogs</Text>
        </View>
        <View>
          <FlatList
            data={this.state.blogData}
            keyExtractor={(item, index) => index.toString()} 
            renderItem={({item}) => 
              <View className="txt-item">
                <Text>{item.title}</Text>
              </View> 
            }
          />
        </View>
      </View>
    );
  }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#eee',
    width:'300px',
    height:'500px'
  },
  statusBar:{
    height:'40px',
    width:'100%',
    backgroundColor:'#99f',
    alignItems: 'center',
    justifyContent: 'center',
  }
})