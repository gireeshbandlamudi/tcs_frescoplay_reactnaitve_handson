module.exports = {
    'App should list all blog title in the same order first blog title should be Beautiful Vue': function (browser) {
        browser
            .url(browser.launch_url)
            .waitForElementVisible('.txt-item', 10000)
            .elements('css selector', '.txt-item', function (result) {
                browser.verify.equal(result.value.length, 8)
                browser.elementIdText(result.value[0].ELEMENT, (ele) => {
                    browser.verify.equal(ele.value, 'Beautiful Vue')
                    browser.end()
                })
            });
    },
    'App should list all blog titles in the same order last blog title should be Vuex Store': function (browser) {
        browser
            .url(browser.launch_url)
            .waitForElementVisible('.txt-item', 10000)
            .elements('css selector', '.txt-item', function (result) {
                browser.verify.equal(result.value.length, 8)
                browser.elementIdText(result.value[7].ELEMENT, (ele) => {
                    browser.verify.equal(ele.value, 'Vuex Store')
                    browser.end()
                })
            });
    },

};